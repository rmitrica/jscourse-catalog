package ro.rmi.catalog.crudrepository;

import org.springframework.data.repository.CrudRepository;

import ro.rmi.catalog.entity.Course;

public interface CourseRepository extends CrudRepository<Course, Integer>{

}
