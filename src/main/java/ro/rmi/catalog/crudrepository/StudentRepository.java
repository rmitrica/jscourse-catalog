package ro.rmi.catalog.crudrepository;

import org.springframework.data.repository.CrudRepository;

import ro.rmi.catalog.entity.Student;

public interface StudentRepository extends CrudRepository<Student, Integer>{

}
