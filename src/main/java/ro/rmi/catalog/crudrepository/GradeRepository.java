package ro.rmi.catalog.crudrepository;

import org.springframework.data.repository.CrudRepository;

import ro.rmi.catalog.entity.Grade;

public interface GradeRepository extends CrudRepository<Grade, Integer>{

}
