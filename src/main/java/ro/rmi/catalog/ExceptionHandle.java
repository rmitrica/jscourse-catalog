package ro.rmi.catalog;

import java.io.Serializable;

import org.springframework.http.HttpStatus;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandle {

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody Message internalServerError(final Exception e) {
		return new Message(e.getMessage());
	}

	@ExceptionHandler({
			IllegalArgumentException.class,
			IllegalStateException.class,
			HttpMediaTypeNotAcceptableException.class
	})
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody Message illegalRequest(final Exception e) {
		return new Message(e.getMessage());
	}
	
	@SuppressWarnings("serial")
	class Message implements Serializable {
		private final String message;

		public Message(final String message) {
			this.message = message;
		}

		public final String getMessage() {
			return message;
		}
	}
}
