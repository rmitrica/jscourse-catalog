package ro.rmi.catalog.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.rmi.catalog.crudrepository.CourseRepository;
import ro.rmi.catalog.entity.Course;

@RestController
@RequestMapping(value = "course")
public class CourseController {

	@Autowired
	private CourseRepository courseRepository;
	
	@RequestMapping(method =  RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Course> getAll(){
		return (List<Course>) courseRepository.findAll();
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Course getOne(@PathVariable Integer id){
		return courseRepository.findOne(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Course create(@RequestBody Course course){
		return courseRepository.save(course);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Course update(@PathVariable Integer id, @RequestBody Course course){
		if (!id.equals(course.getId())){
			throw new IllegalArgumentException(); 
		}
		return courseRepository.save(course);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Integer id){
		courseRepository.delete(id);
	}
}
