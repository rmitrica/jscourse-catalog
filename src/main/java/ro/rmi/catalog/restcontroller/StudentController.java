package ro.rmi.catalog.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.rmi.catalog.crudrepository.StudentRepository;
import ro.rmi.catalog.entity.Student;

@RestController
@RequestMapping(value = "student")
public class StudentController {

	@Autowired
	private StudentRepository studentRepository;
	
	@RequestMapping(method =  RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Student> getAll(){
		List<Student> result = (List<Student>) studentRepository.findAll();
		System.out.println(result);
		return result;
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Student getOne(@PathVariable Integer id){
		Student result =studentRepository.findOne(id);
		System.out.println(result);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Student create(@RequestBody Student student){
		return studentRepository.save(student);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Student update(@PathVariable Integer id, @RequestBody Student student){
		if (!id.equals(student.getId())){
			throw new IllegalArgumentException(); 
		}
		return studentRepository.save(student);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Integer id){
		studentRepository.delete(id);
	}
}
